<?php

namespace Militaruc\Ptwo\App\Providers;

use Illuminate\Support\ServiceProvider;

class PtwoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '../../../routes/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register controllers
        //$this->app->make('Militaruc\Pone\App\Http\Controllers\PoneController');


        // register views
        //$this->loadViewsFrom(__DIR__.'../../../resources/views', 'testpackageviews');
    }
}
