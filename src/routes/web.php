<?php
Route::get('p-two', function(){
    echo 'Testing route for package P-two!<br>';
    echo 'Package version 1.1<br>';
});
Route::get('p-two/1/2', function(){
    echo 'Testing route for package P-two!<br>';
    echo 'Package version 1.2<br>';
});